load data 
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized_movie_db.csv"
into table denormalized
fields terminated by ';'; #fields yerine columns da diyebilirsin

show variables like "secure_file_priv";

insert  into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinct movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

insert  into genres (movie_id, genre_name)
select distinct movie_id, genre_name
from denormalized;

insert  into languages(movie_id, language_name)
select distinct movie_id, language_name
from denormalized;


select producer_country_id, producer_country_name
from denormalized
union distinct
select star_country_id, star_country_name
from denormalized
union distinct
select director_country_id, director_country_name
from denormalized;

insert  into countries(country_id, country_name)
select distinct country_id, country_name
from denormalized;

insert  into stars(star_id, country_id, star_name)
select distinct star_id, country_id, star_name
from denormalized;

insert  into directors(director_id, country_id, director_name)
select distinct director_id, country_id, director_name
from denormalized;

insert  into movie_stars(movie_id, star_id)
select distinct movie_id, star_id
from denormalized;

insert  into movie_directors(movie_id, director_id)
select distinct movie_id, director_id
from denormalized;

insert  into producer_countries(movie_id, country_id)
select distinct movie_id, country_id
from denormalized;