#What is the average value in population density?
SELECT AVG(p_density) AS avg
FROM population;

#What is the number of countries?
SELECT COUNT(country_name) AS country_total
FROM country;

#What is the maximum value in food production indexes?
SELECT MAX(food_index) AS max_food_index
FROM production_index;

#What is the minimum value in unemployment?
SELECT MIN(unemployment) AS min_unemployment
FROM employment;

#What is the maximum life expectancy at birth rate among women ?
SELECT MAX(life_expectancy_at_birth.female) AS max_f
FROM health ;

#What is average value of individual using the internet in all countries?
SELECT AVG(individuals_using_internet) AS avg_using_int
FROM population;

#What is the minimum value of CO2 emmision estimates?
SELECT MIN(CO2_emission_estimates) AS min_CO2_emmission
FROM environment;

#What is the average fertility rate in Western Europe?
SELECT AVG(fertility_rate) 
FROM health 
WHERE region='Western Europe';

#What is the maximum  infant mortality rate in Eastern Asia?
SELECT MAX(infant_mortality_rate) 
AS maximum_infant_mortality_rate 
FROM health 
WHERE region ='Eastern Asia' ;

#What is the average value in pop. using improved drinking water (rural, %) in Costa Rica?
SELECT AVG(using_improved_drinking_water.rural)
FROM environment
WHERE country_name='Costa Rica';